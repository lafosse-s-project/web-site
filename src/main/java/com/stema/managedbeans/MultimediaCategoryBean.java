/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stema.managedbeans;

import com.stema.ejb.MultimediaCategoryEJB;
import com.stema.javabeans.MultimediaCategory;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.enterprise.context.RequestScoped;

/**
 *
 * @author samli
 */
@Named
@RequestScoped //attribus dans la requete http
public class MultimediaCategoryBean {

    //modele et acces à mongodb
    @EJB
    private MultimediaCategoryEJB multimediaCategoryEJB;
    
    
    /**
     * Creates a new instance of MultimediaCategoryBean
     */
    public MultimediaCategoryBean() { 
    
    }
    
        public void create(){
        //POJO    
        MultimediaCategory mc=new MultimediaCategory();
        mc.setName("Video 360°");
        
        //Modele
        multimediaCategoryEJB.create(mc);
    }
    
}

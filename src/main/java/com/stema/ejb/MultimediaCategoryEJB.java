/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stema.ejb;

import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Updates.combine;
import static com.mongodb.client.model.Updates.set;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import com.stema.javabeans.MultimediaCategory;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;

/**
 *
 * @author jerom
 */
@Stateless
public class MultimediaCategoryEJB {

    //injecter le provider MongoDB
    @EJB
    private MongoClientProvider mongoClientProvider;

    // variables mongodb
    private MongoClient mongoClient;
    private MongoDatabase mongoDatabase;
    private MongoCollection collection;

    @PostConstruct
    public void init() {
        mongoClient = mongoClientProvider.getMongoClient();
        mongoDatabase = mongoClient.getDatabase("vrproject");
        collection = mongoDatabase.getCollection("MultimediaCategory");
    }

    // modifier un objet
    public Long udpate(MultimediaCategory mc) {
        Bson cle = eq("_id", new ObjectId(mc.getId()));
        Bson updateName = set("name", mc.getName());
        Bson updates = combine(updateName);

        UpdateResult res = collection.updateOne(cle, updates);
        return res.getMatchedCount();
    }

    //creer
    public void create(MultimediaCategory mc) {
        Document document = new Document();
        document.put("name", mc.getName());

        collection.insertOne(document);
    }

    // supprimer un objet
    public Long delete(String id) {
        Bson cle = eq("_id", new ObjectId(id));
        DeleteResult res = collection.deleteOne(cle);
        return res.getDeletedCount();
    }

    // lire un objet
    public MongoCursor<Document> read() {
        FindIterable<Document> iterable = collection.find();
        MongoCursor<Document> cursor = iterable.iterator();

        return cursor;
    }

    // wrapper Cursor vers List<DataSonde>
    public List<MultimediaCategory> wrapper(MongoCursor<Document> cursor) {
        List<MultimediaCategory> listeMultimediaCategory = new ArrayList<>();
        while (cursor.hasNext()) {
            MultimediaCategory mc = new MultimediaCategory();
            Document obj = cursor.next();

            if (obj != null) {
                if (obj.get("_id") != null) {
                    mc.setId(obj.get("_id").toString());
                }
                if (obj.get("name") != null) {
                    mc.setName(obj.get("hygro").toString());
                }
                listeMultimediaCategory.add(mc);
            }
        }
        return listeMultimediaCategory;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stema.javabeans;

import java.io.Serializable;

/**
 *
 * @author samli
 */
public class MultimediaCategory implements Serializable { // implements Serializable, regarde si c'est bien codé
    
    private String name;
    private String _id;

    public MultimediaCategory() {
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return _id;
    }

    public void setId(String _id) {
        this._id = _id;
    }
    
    
    
    
    
    
}
